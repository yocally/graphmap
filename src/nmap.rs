use serde::{Serialize, Deserialize};
use std::path::Path;
use std::fs::read_to_string;


#[derive(Serialize, Deserialize)]
pub struct Nmap {
    pub nmaprun: Nmaprun,
}

#[derive(Serialize, Deserialize)]
pub struct Nmaprun {
    #[serde(rename = "@args")]
    pub args: String,
    pub host: Host,
}

#[derive(Serialize, Deserialize)]
pub struct Host {
    pub address: Vec<Address>,
    pub hostnames: Hostnames,
    pub ports: Ports,
}

#[derive(Serialize, Deserialize)]
pub struct Address {
    #[serde(rename = "@addr")]
    pub addr: String,
    #[serde(rename = "@addrtype")]
    pub addrtype: String,
}

#[derive(Serialize, Deserialize)]
pub struct Hostnames {
    pub hostname: Vec<Hostname>,
}

#[derive(Serialize, Deserialize)]
pub struct Hostname {
    #[serde(rename = "@name")]
    pub name: String,
    #[serde(rename = "@type")]
    pub hostname_type: String,
}

#[derive(Serialize, Deserialize)]
pub struct Ports {
    pub port: Vec<Port>,
}

#[derive(Serialize, Deserialize)]
pub struct Port {
    #[serde(rename = "@protocol")]
    pub protocol: String,
    #[serde(rename = "@portid")]
    pub portid: String,
    pub state: State,
    pub service: Service,
}

#[derive(Serialize, Deserialize)]
pub struct Service {
    #[serde(rename = "@name")]
    pub name: String,
    #[serde(rename = "@product")]
    pub product: Option<String>,
    #[serde(rename = "@version")]
    pub version: String,
    #[serde(rename = "@extrainfo")]
    pub extrainfo: String,
    #[serde(rename = "@method")]
    pub method: String,
    #[serde(rename = "@conf")]
    pub conf: String,
    pub cpe: Option<String>,
}

#[derive(Serialize, Deserialize)]
pub struct State {
    #[serde(rename = "@state")]
    pub state: String,
    #[serde(rename = "@reason")]
    pub reason: String,
    #[serde(rename = "@reason_ttl")]
    pub reason_ttl: String,
}


impl Nmap {
    pub fn from_file<P: AsRef<Path>>(path: P) -> Result<Nmap, Box<dyn std::error::Error + 'static>> {
        let contents = read_to_string(path)?;
        Ok(serde_json::from_str(&contents)?)
    }

    pub fn get_hostname(&self) -> Option<String> {
        match self.nmaprun.host.hostnames.hostname.get(0) {
            Some(e) => Some(e.name.clone()),
            None => None,
        }
    }

    pub fn get_ip(&self) -> Option<String> {
        match self.nmaprun.host.address.get(0) {
            Some(e) => Some(e.addr.clone()),
            None => None,
        }
    }

    pub fn to_query(&self) -> String {
        let mut query = format!("MERGE (m:Machine {{name: \"{}\", ip: \"{}\"}})\n", self.get_hostname().unwrap(), self.get_ip().unwrap());
        for i in &self.nmaprun.host.ports.port {
            query = format!("{}MERGE (m)-[:LISTEN]->(p{}:Port {{name: \"{}/{}\"}})\n", query, i.portid, i.portid, i.protocol);
            query = format!("{}MERGE (p{})-[:EXPOSES]->(s{}:Service {{name: \"{}\"}})\n", query, i.portid, i.portid, i.service.name);
        }
        query
    }
}