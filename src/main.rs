extern crate serde;
extern crate serde_json;

mod nmap;


fn main() {
    let obj = nmap::Nmap::from_file("sample.json").unwrap();
    println!("{}\n{}", obj.get_hostname().unwrap(), obj.get_ip().unwrap());
    println!("QUERY:");
    println!("{}", obj.to_query());
}